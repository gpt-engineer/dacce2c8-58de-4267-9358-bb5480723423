// Selecting elements
const generateBtn = document.getElementById("generate-btn");
const questionP = document.getElementById("question");
const answerP = document.getElementById("answer");
const formattedP = document.getElementById("formatted");

// Event listener for button
generateBtn.addEventListener("click", () => {
  console.log("Button clicked");
  fetch("https://filipstal.pythonanywhere.com/randomqa")
    .then((response) => {
      console.log("Response received", response);
      return response.json();
    })
    .then((data) => {
      console.log("Data received", data);
      questionP.textContent = data.question;
      answerP.textContent = data.answer;
      formattedP.textContent = data.formatted;
    })
    .catch((error) => console.error("Error:", error));
});
